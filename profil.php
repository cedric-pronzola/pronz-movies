<?php 

    session_start(); require_once './connect/config.php'; include './connect/connexion.php'; include './connect/function.php'; 

    if (isset($_SESSION['pseudo']) && isset($_SESSION['password']) OR isset($_COOKIE['connect_cinefa']))
    {
        $se_deconnecter = '<p><a href="./logout.php">Se déconnecter</a></p>';

        $autorize_pseudo = $_COOKIE['connect_cinefa'];
        $access = 1;
    }
    else
    {
        $se_connecter = '<p>Accès refusé. Vous n\'êtes pas connecté ! <a href="./index.php">Se connecter</a> ou <a href="./index.php">s\'enregistrer</a></p>';

        $autorize_pseudo ='';
        $access = 0;
    }

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Espace Membre | Cinefa</title>
        <link rel="shortcut icon" href="./img/movies.ico" type="image/x-icon">

        <link rel="stylesheet" href="./script/style.css">
        <link type="text/css" rel="stylesheet" href="./script/materialize/css/materialize.min.css"  media="screen,projection"/>

    </head>
    <body class="container-fluid">

        <nav>
        <div class="nav-wrapper">
            <a href="./index.php" class="brand-logo">Accueil</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
            <li><a href="./pages/directors.php">Réalisateurs</a></li>
            <li><a href="./pages/actors.php">Acteurs</a></li>
            <li><a href="./pages/movies.php">Films</a></li>
            </ul>
        </div>
        </nav>

        <ul class="sidenav" id="mobile-demo">
            <li><a href="./pages/directors.php">Réalisateurs</a></li>
            <li><a href="./pages/actors.php">Acteurs</a></li>
            <li><a href="./pages/movies.php">Films</a></li>
        </ul>


        <?php

            if ($access) 
            {
                echo $se_deconnecter;
                echo "<p>Bienvenue dans votre espace membre " . $autorize_pseudo . '.</p>' ;

                $what_id = "SELECT users.id_user
                FROM users
                WHERE users.pseudo = '$autorize_pseudo'";

                $what_id_query = mysqli_query($db_connexion, $what_id);

                $response_what_id = mysqli_fetch_assoc($what_id_query);

                $user_id = $response_what_id['id_user'];

                $which_cat = "SELECT categories.id_category, categories.id_user, categories.category_title
                FROM categories
                WHERE id_user = '$user_id'";

                $which_cat_query = mysqli_query($db_connexion, $which_cat);

                if ($db_select) 
                {
                    $list_fav = '';
                    $i = 0;
                    
                    echo '
                    <div class="row">
                        <form action="" method="post">
                            <div class="input-field col s8  l3">
                                <select name="list_choice">
                                    <option value="" disabled selected>Choix de la liste</option>' . "\n";

                    while ($response_wich_cat = mysqli_fetch_assoc($which_cat_query)) 
                    {
                        $list_fav = $response_wich_cat['category_title'];
                        echo '      <option value="'.$response_wich_cat['id_category'].'">'. $list_fav . '</option>' . "\n";
                    }

                        echo '  </select>
                                <label>Liste de favoris</label>
                            </div>
                            <div class="col s4 l3">
                                <button data-tooltip="Accéder à la liste" class="btn-floating btn-large waves-effect waves-light tooltipped" type="submit" name="valider">
                                    <i class="material-icons right">check</i>
                                </button>
                            </div>
                            <div class="col s4 l3">
                                <button data-tooltip="Modifier la liste" class="btn-floating tooltipped btn-large" type="submit" name="edit_cat">
                                    <i class="material-icons right">edit</i>
                                </button>
                                <button onclick="return confirm(\'Voulez-vous supprimer ?\')" data-tooltip="Supprimer la liste" class="btn modal-trigger btn-floating btn tooltipped btn-large waves-effect waves-light red" type="submit" name="del_cat">
                                    <i class="material-icons">delete_forever</i> 
                                </button>
                            </div>
                        </form>
                    </div>';
                }

                if (isset($_POST['valider']) AND isset($_POST['list_choice'])) 
                {

                    // Quand je clique sur valider

                    $list_choice = $_POST['list_choice'];

                    $check_list2 = "SELECT categories.*, DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, users.id_user
                    FROM categories
                    INNER JOIN users ON users.id_user = categories.id_user
                    WHERE categories.id_category = '$list_choice' AND users.pseudo = '$autorize_pseudo'";

                    $check_list = "SELECT movies.*, category_content.*, categories.*, 
                    DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, 
                    DATE_FORMAT(movies.release_date, '%d-%m-%Y') date, genre.*, 
                    directors.id_director, CONCAT(directors.first_name, ' ', directors.last_name) AS director_name, users.id_user
                    FROM movies
                    INNER JOIN category_content ON category_content.id_movie = movies.id_movie
                    INNER JOIN categories ON categories.id_category = category_content.id_category
                    INNER JOIN users ON users.id_user = categories.id_user
                    INNER JOIN genre_movies ON genre_movies.id_movie = movies.id_movie
                    INNER JOIN genre ON genre.id_genre = genre_movies.id_genre
                    INNER JOIN directors ON directors.id_director = movies.id_director
                    WHERE categories.id_category = '$list_choice' AND categories.id_user = '$user_id'
                    GROUP BY movies.id_movie";

                    $check_list_query = mysqli_query($db_connexion, $check_list);

                    $check_list_query2 = mysqli_query($db_connexion, $check_list2);

                    if ($db_select) 
                    {
                        $check = mysqli_fetch_assoc($check_list_query2);
                    }

                    echo '<h5 style="margin-top:80px;"> Nom du favoris  : ' . $check['category_title'] . '<p> Créé le : ' . $check['creation'] . '</h5>
                    
                        <div class="row">
                            <table class="highlight responsive-table col s8 m10">  
                                <thead>
                                    <tr>
                                        <th>Titre du film</th>
                                        <th>Nom du réalisateur</th>
                                        <th>Genre</th>
                                        <th>Date de sortie en salle</th>
                                        <th>Affiche</th>
                                        <th>Supprimer</th>
                                    </tr>
                                </thead>
                                <tbody>';
                            
                    if ($db_select) 
                    {
                        while ($response_check_list = mysqli_fetch_assoc($check_list_query)) 
                        {
                            echo '<tr><td> <a href="./pages/fiche_movies.php?id='. $response_check_list['id_movie'] . '">'.  $response_check_list['title'] . '</a></td>' . "\n" .
                            '<td> <a href="./pages/fiche.php?id=' . $response_check_list['id_director'] . '&work=directors&bdd=id_director">' . $response_check_list['director_name'] . '</a></td>' ."\n" .
                            '<td>' . $response_check_list['name'] . '</td>' ."\n" .
                            '<td>' . $response_check_list['date'] . '</td>' ."\n" .
                            '<td>' . ' <img class="materialboxed" width="90" src="./img/movies/'. $response_check_list['poster'] .'"> </td>' . "\n" .
                            '<td>' . '<form action="" method="post"> <input type="hidden" id="'.$response_check_list['id_category'] .'" name="to_del_movie" value="'.$response_check_list['id_category'] .'"> 
                            <button onclick="return confirm(\'Voulez-vous supprimer ?\')" value="'.$response_check_list['id_movie'] .'" name="delete" 
                            data-tooltip="Supprimer le film de la liste" class="btn-floating btn tooltipped 
                            btn-large waves-effect waves-light red"> <i class="material-icons">delete_forever</i> </button> </form></td></tr>';
                        }

                        echo '  </tbody>
                            </table>
                        </div>';

                    }

                }
                elseif (isset($_POST['valider']) AND !isset($_POST['list_choice']))
                {
                    echo "Veuillez choisir une catégorie";
                }

                // Effacer la catégorie

                if (isset($_POST['del_cat']) AND isset($_POST['list_choice'])) 
                {
                    $list_choice = $_POST['list_choice'];

                    $del_cat = "DELETE FROM categories
                    WHERE id_category = '$list_choice' AND id_user = '$user_id'";

                    $del_cat_query = mysqli_query($db_connexion, $del_cat);

                    if ($db_select) 
                    {
                        if ($del_cat_query) 
                        {
                            echo '<script type="text/javascript">
                            document.location.href="./profil.php";
                            </script>';
                        }
                        else
                        {
                            echo "erreur";
                        }
                    }

                }
                elseif (isset($_POST['del_cat']) AND !isset($_POST['list_choice']))
                {
                    echo "Veuillez choisir une catégorie";
                }

                // Modifier la catégorie

                if (isset($_POST['edit_cat']) AND isset($_POST['list_choice'])) 
                {

                    $list_choice = $_POST['list_choice'];

                    $check_list2 = "SELECT categories.*, DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, users.id_user
                    FROM categories
                    INNER JOIN users ON users.id_user = categories.id_user
                    WHERE categories.id_category = '$list_choice' AND users.pseudo = '$autorize_pseudo'";

                    $check_list = "SELECT movies.*, category_content.*, categories.*, 
                    DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, 
                    DATE_FORMAT(movies.release_date, '%d-%m-%Y') date, genre.*, 
                    directors.id_director, CONCAT(directors.first_name, ' ', directors.last_name) AS director_name, users.id_user
                    FROM movies
                    INNER JOIN category_content ON category_content.id_movie = movies.id_movie
                    INNER JOIN categories ON categories.id_category = category_content.id_category
                    INNER JOIN users ON users.id_user = categories.id_user
                    INNER JOIN genre_movies ON genre_movies.id_movie = movies.id_movie
                    INNER JOIN genre ON genre.id_genre = genre_movies.id_genre
                    INNER JOIN directors ON directors.id_director = movies.id_director
                    WHERE categories.id_category = '$list_choice' AND categories.id_user = '$user_id'
                    GROUP BY movies.id_movie";

                    $check_list_query = mysqli_query($db_connexion, $check_list);

                    $check_list_query2 = mysqli_query($db_connexion, $check_list2);

                    if ($db_select) 
                    {
                        $check = mysqli_fetch_assoc($check_list_query2);
                    }

                    echo    '<h5 style="margin-top:80px;"> Modification de la liste  
                                <span class="blue-text text-darken-2"> &laquo;' . $check['category_title'] . '&raquo; </span> 
                            </h5>
                            <div class="row">
                                <form action="" method="post" class="col s3 l3"> 
                                    <div class="row">
                                        <input type="text" name="list_name" id="input_text" class="validate" 
                                        minlength="3" maxlength="30" data-length="30" value="' . $check['category_title'] . '"> 
                                        <label for="input_text">Choisissez un nouveau nom</label>
                                        <input type="hidden" name="list_choice" value="' . $check['id_category'] . '">
                                    </div>
                                    <button class="btn-floating btn-large s2 m2 l3 waves-light red" type="submit" name="edit_name">
                                        <i class="material-icons right">check</i>
                                    </button>
                                </form> 
                            </div>';

                    if (isset($_POST['edit_name']))
                    {
                        echo "edit";
                    }

                }
                elseif (isset($_POST['edit_cat']) AND !isset($_POST['list_choice']))
                {
                    echo "Veuillez choisir une catégorie";
                }

                    // Modification du nom de la liste

                if (isset($_POST['edit_name'])) 
                {
                    $list_name = protect($_POST['list_name']);
                    $list_choice = $_POST['list_choice'];

                    $check_list2 = "SELECT categories.*, DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, users.id_user
                    FROM categories
                    INNER JOIN users ON users.id_user = categories.id_user
                    WHERE categories.id_category = '$list_choice' AND users.id_user = '$user_id'";

                    $check_list = "SELECT movies.*, category_content.*, categories.*, 
                    DATE_FORMAT(categories.creation_date, '%d-%m-%Y') creation, 
                    DATE_FORMAT(movies.release_date, '%d-%m-%Y') date, genre.*, 
                    directors.id_director, CONCAT(directors.first_name, ' ', directors.last_name) AS director_name, users.id_user
                    FROM movies
                    INNER JOIN category_content ON category_content.id_movie = movies.id_movie
                    INNER JOIN categories ON categories.id_category = category_content.id_category
                    INNER JOIN users ON users.id_user = categories.id_user
                    INNER JOIN genre_movies ON genre_movies.id_movie = movies.id_movie
                    INNER JOIN genre ON genre.id_genre = genre_movies.id_genre
                    INNER JOIN directors ON directors.id_director = movies.id_director
                    WHERE categories.id_category = '$list_choice' AND categories.id_user = '$user_id'
                    GROUP BY movies.id_movie";

                    $check_list_query = mysqli_query($db_connexion, $check_list);

                    $check_list_query2 = mysqli_query($db_connexion, $check_list2);

                    if ($db_select) 
                    {
                        $check = mysqli_fetch_assoc($check_list_query2);
                    }
                        
                        if ($db_select) 
                        {
                            $rename_cat = "UPDATE categories
                            SET categories.category_title = '$list_name'
                            WHERE categories.id_category = '$list_choice' AND categories.id_user = '$user_id'";

                            $rename_cat_query = mysqli_query($db_connexion, $rename_cat);

                            if ($rename_cat_query) 
                            {
                                echo "Votre modification a bien été prise en compte";
                            }
                        }
                }

                    // Effacer un film de la catégorie

                if (isset($_POST['delete'])) 
                {
                    $delete_movie_post = $_POST['delete'];

                    $to_del_movie = $_POST['to_del_movie'];

                    $supp_movie = "DELETE FROM category_content
                    WHERE id_movie = '$delete_movie_post' AND id_category = '$to_del_movie'";

                    $delete_movie_post_query = mysqli_query($db_connexion, $supp_movie);

                    if ($db_select) 
                    {
                        if ($delete_movie_post_query) 
                        {
                            echo "<p>Votre film a bien été supprimé</p>";
                        }
                        else
                        {
                            echo "Erreur";
                        }
                    }
                    
                }           // Création d'une nouvelle liste

                    echo '<div class="row">
                            <h5 class="col s10" style="margin-top:150px;">Créer une nouvelle liste</h5>
                        
                            <form class="col s10" action="" method="post">
                                <div class="row">
                                    <div class="input-field col s9 m9">
                                        <i class="material-icons prefix">playlist_add</i>
                                        <input type="text" name="list_name" id="input_text" class="validate" minlength="3" maxlength="30" data-length="30" required>
                                        <label for="input_text">Nom de la liste</label>
                                        <span class="helper-text">30 caractères maximum</span>
                                    </div>
                                        <button class="btn-floating btn-large s2 m4" type="submit" name="create_list">
                                            <i class="material-icons right">add</i>
                                        </button>
                                </div>
                            </form>
                        </div>';
            
                if (isset($_POST['create_list'])) 
                {

                    $list_name = $_POST['list_name'];

                    $protect_list_name = protect($list_name); // Ligne 4 dans la page "./connect/function.php" 

                    if ($db_select) 
                    {
                        $add_list = "INSERT INTO categories(id_user, category_title, creation_date)
                            VALUES ('$user_id', '$protect_list_name', CURDATE())";

                        $add_list_query = mysqli_query($db_connexion, $add_list);

                        if ($add_list_query) 
                        {
                            echo '<script type="text/javascript">
                            document.location.href="./profil.php";
                            </script>';

                            echo "Votre liste a été créée";
                        }
                        else 
                        {
                            echo "erreur";
                        }
                    }
                }
            }
            else
            {
                echo $se_connecter;
            }
        
                mysqli_close($db_connexion);

        ?>
        <script type="text/javascript" src="./script/jquery.js"></script>  
        <script type="text/javascript" src="./script/materialize/js/materialize.min.js"></script>
        <script type="text/javascript" src="./script/main.js"></script>

        <script>
            $(document).ready(function(){
            $('.modal').modal();
            });
        </script>

    </body>
</html>